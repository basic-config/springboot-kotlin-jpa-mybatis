package net.suby.springbootkotlinjpamybatis

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringbootKotlinJpaMybatisApplication

fun main(args: Array<String>) {
    runApplication<SpringbootKotlinJpaMybatisApplication>(*args)
}
