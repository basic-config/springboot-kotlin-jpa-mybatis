package net.suby.springbootkotlinjpamybatis.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import java.util.*
import javax.sql.DataSource


@Configuration
@EnableJpaRepositories(
        basePackages = ["net.suby.springbootkotlinjpamybatis.**.repository.jpa"],
        entityManagerFactoryRef = "jpaEntityManager",
        transactionManagerRef = "jpaTransactionManager"
)
class PersistenceJpaConfig(val env: Environment) {

    @Bean(name = ["jpaDataSource"])
    @Primary
    @ConfigurationProperties(prefix = "spring.jpa.datasource")
    fun jpaDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Primary
    @Bean
    fun jpaEntityManager(): LocalContainerEntityManagerFactoryBean {
        val em = LocalContainerEntityManagerFactoryBean()
        em.dataSource = jpaDataSource()
        em.setPackagesToScan("net.suby.springbootkotlinjpamybatis.**.domain")
        val vendorAdapter = HibernateJpaVendorAdapter()
        em.jpaVendorAdapter = vendorAdapter
        val properties = HashMap<String, Any?>()
        properties["hibernate.hbm2ddl.auto"] = env.getProperty("hibernate.hbm2ddl.auto")
        properties["hibernate.dialect"] = env.getProperty("hibernate.dialect")
        em.setJpaPropertyMap(properties)
        return em
    }

    @Primary
    @Bean
    fun jpaTransactionManager(): PlatformTransactionManager? {
        val transactionManager = JpaTransactionManager()
        transactionManager.entityManagerFactory = jpaEntityManager().getObject()
        return transactionManager
    }
}