package net.suby.springbootkotlinjpamybatis.config

import org.apache.ibatis.session.SqlSessionFactory
import org.mybatis.spring.SqlSessionFactoryBean
import org.mybatis.spring.SqlSessionTemplate
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.sql.DataSource

@Configuration
@MapperScan(value= ["net.suby.springbootkotlinjpamybatis.**.repository.mybatis"], sqlSessionFactoryRef="myBatisSqlSessionFactory")
@EnableTransactionManagement
class PersistenceMybatisConfig {

    @Bean(name=["myBatisDataSource"])
    @ConfigurationProperties(prefix = "spring.mybatis.datasource")
    fun myBatisDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Bean(name=["myBatisSqlSessionFactory"])
    @Throws(Exception::class)
    fun myBatisSqlSessionFactory(@Qualifier("myBatisDataSource") myBatisDataSource: DataSource, applicationContext : ApplicationContext) : SqlSessionFactory? {
        val sqlSessionFactoryBean : SqlSessionFactoryBean = SqlSessionFactoryBean()
        sqlSessionFactoryBean.setDataSource(myBatisDataSource)
        sqlSessionFactoryBean.setMapperLocations(*applicationContext.getResources("classpath:mapper/user/*.xml"))
        return sqlSessionFactoryBean.getObject()
    }

    @Bean(name=["myBatisSqlSessionTemplate"])
    @Throws(Exception::class)
    fun myBatisSqlSessionTemplate (myBatisSqlSessionFactory : SqlSessionFactory): SqlSessionTemplate {
        return SqlSessionTemplate(myBatisSqlSessionFactory)
    }

}