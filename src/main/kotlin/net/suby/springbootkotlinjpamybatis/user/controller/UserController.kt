package net.suby.springbootkotlinjpamybatis.user.controller

import net.suby.springbootkotlinjpamybatis.user.domain.UserVO
import net.suby.springbootkotlinjpamybatis.user.service.UserService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/users")
class UserController(val userService : UserService) {

    @RequestMapping("/{id}")
    fun findByUserId(@PathVariable id: String){
        var parameter = HashMap<String, String>()
        parameter["id"] = id

        // mybatis
        var result = userService.findUserId(parameter)
        println(result)

        // jpa
        var userVo : UserVO = userService.getUser(id);
        println(userVo)


    }

}