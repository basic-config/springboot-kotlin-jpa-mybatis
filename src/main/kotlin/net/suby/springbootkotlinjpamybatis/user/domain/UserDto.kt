package net.suby.springbootkotlinjpamybatis.user.domain

import java.sql.Date
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity(name = "users")
class UserVO {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    var userId: String = ""
    @Column(name = "password")
    var password: String = ""
    @Column(name = "user_nm")
    var userNm: String = ""
    @Column(name = "user_role")
    var userRole: String =""
    @Column(name = "creative_dt")
    var creativeDt: LocalDateTime = LocalDateTime.now()

}