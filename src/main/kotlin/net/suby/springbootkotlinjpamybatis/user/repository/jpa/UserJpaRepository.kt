package net.suby.springbootkotlinjpamybatis.user.repository.jpa

import net.suby.springbootkotlinjpamybatis.user.domain.UserVO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface UserJpaRepository : JpaRepository<UserVO?, String?>
