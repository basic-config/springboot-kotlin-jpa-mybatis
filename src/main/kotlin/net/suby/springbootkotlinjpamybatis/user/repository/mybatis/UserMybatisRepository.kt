package net.suby.springbootkotlinjpamybatis.user.repository.mybatis

import org.springframework.stereotype.Repository
import kotlin.collections.HashMap

@Repository
open interface UserMybatisRepository {
    open fun findUserId(parameter: HashMap<String, String>): HashMap<String?, String?>?
}