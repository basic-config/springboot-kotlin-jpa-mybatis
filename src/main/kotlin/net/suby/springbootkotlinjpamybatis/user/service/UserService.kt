package net.suby.springbootkotlinjpamybatis.user.service

import net.suby.springbootkotlinjpamybatis.user.domain.UserVO
import net.suby.springbootkotlinjpamybatis.user.repository.jpa.UserJpaRepository
import net.suby.springbootkotlinjpamybatis.user.repository.mybatis.UserMybatisRepository
import org.springframework.stereotype.Service
import kotlin.collections.HashMap


@Service
class UserService(val userMybatisRepository: UserMybatisRepository, val userJpaRepository: UserJpaRepository) {
    fun findUserId(parameter: HashMap<String, String>): HashMap<String?, String?>? {
        return userMybatisRepository.findUserId(parameter)
    }

    fun getUser(id: String): UserVO {
        return userJpaRepository.getOne(id)
    }


}