CREATE TABLE `users` (
  `user_id` varchar(32) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `user_nm` varchar(32) DEFAULT NULL,
  `user_role` varchar(32) DEFAULT NULL,
  `creative_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO users
  (user_id, password, user_nm, user_role, creative_dt)
VALUES
  ('myborn', '123456', '탄생', 'ADMIN', 2020-01-30);

INSERT INTO users
  (user_id, password, user_nm, user_role, creative_dt)
VALUES
  ('user', '123789', '사용자', 'USER', 2020-01-30);

